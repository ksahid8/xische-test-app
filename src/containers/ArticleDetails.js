import Tag from "components/Tag";
import React from "react";
import {
  ARTICLE_DETAIL_IMAGE,
  IMAGE_TYPES,
  formatDate,
  getImageUrl,
  keywordsFormatter,
} from "utils/constants";

const ArticleDetail = ({ article }) => {
  const {
    title,
    abstract,
    published_date,
    section,
    subsection,
    byline,
    media,
    adx_keywords,
    des_facet,
    org_facet,
    per_facet,
    geo_facet,
  } = article;

  const keywords = keywordsFormatter(adx_keywords);
  const image = getImageUrl(media, IMAGE_TYPES.detail) ?? ARTICLE_DETAIL_IMAGE;

  return (
    <div className="detail-card" data-testid="article-details">
      <h3 className="detail-title">{title}</h3>
      <div className="image-container">
        <img src={image} alt={title} className="detail-image" />
        <div className="section-container">
          <div className="section-text">
            {section}
            {subsection && (
              <div className="sub-section-text">({subsection})</div>
            )}
          </div>
        </div>
      </div>
      <div className="detail-info-header">
        <div className="detail-info-published">
          Published on {formatDate(published_date)}
        </div>
        <div className="detail-info-by-line">{byline}</div>
      </div>
      <p className="detail-text">{abstract}</p>

      {des_facet?.length > 0 && (
        <div className="detail-section">
          <h4 className="detail-subtitle">Descriptive Terms:</h4>
          <div className="detail-tags">
            {des_facet.map((item, index) => (
              <Tag key={index} item={item} />
            ))}
          </div>
        </div>
      )}

      {org_facet?.length > 0 && (
        <div className="detail-section">
          <h4 className="detail-subtitle">Organizations:</h4>
          <div className="detail-tags">
            {org_facet.map((item, index) => (
              <Tag key={index} item={item} />
            ))}
          </div>
        </div>
      )}

      {per_facet?.length > 0 && (
        <div className="detail-section">
          <h4 className="detail-subtitle">Persons:</h4>
          <div className="detail-tags">
            {per_facet.map((item, index) => (
              <Tag key={index} item={item} />
            ))}
          </div>
        </div>
      )}

      {geo_facet?.length > 0 && (
        <div className="detail-section">
          <h4 className="detail-subtitle">Locations:</h4>
          <div className="detail-tags">
            {geo_facet.map((item, index) => (
              <Tag key={index} item={item} />
            ))}
          </div>
        </div>
      )}

      {keywords && (
        <div className="detail-section">
          <h4 className="detail-subtitle">Keywords:</h4>
          <p className="detail-info-keywords">{keywords}</p>
        </div>
      )}
    </div>
  );
};

export default ArticleDetail;
