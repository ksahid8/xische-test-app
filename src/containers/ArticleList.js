import ListCard from "components/ListCard";
import React from "react";

const ArticleList = ({ articles, onArticleClick, selectedArticle }) => {
  return (
    <ul className="article-list" data-testid="article-list">
      {articles?.map((article) => (
        <ListCard
          selectedArticle={selectedArticle}
          key={article?.id}
          article={article}
          onArticleClick={onArticleClick}
        />
      ))}
    </ul>
  );
};

export default ArticleList;
