import React, { useState, useEffect } from "react";
import ArticleDetail from "containers/ArticleDetails";
import ArticleList from "containers/ArticleList";
import { API_BASE_URL, API_KEY, PERIOD } from "utils/constants";
import { getArticles } from "utils/api";
import Loader from "components/Loader";
import "App.css";

function App() {
  const [articles, setArticles] = useState([]);
  const [selectedArticle, setSelectedArticle] = useState(null);
  const [loading, setLoading] = useState(true);

  const fetchUrl = `${API_BASE_URL}/${PERIOD}.json?api-key=${API_KEY}`;

  useEffect(() => {

    const fetchArticles = async () => {
      const response = await getArticles(fetchUrl);
      if (response?.status === "OK") {
        setArticles(response?.results);
        setSelectedArticle(response?.results[0]);
      }
      setLoading(false);
    };
    
    fetchArticles();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleArticleClick = (id) => {
    setSelectedArticle(articles.find((article) => article.id === id));
  };

  return (
    <div className="main-wrapper" data-testid="main-wrapper">
      <div className="list-wrapper" data-testid="list-wrapper">
        <h2>Most Popular</h2>
        {loading ? (
          <Loader />
        ) : (
          <ArticleList
            selectedArticle={selectedArticle}
            articles={articles}
            onArticleClick={handleArticleClick}
          />
        )}
      </div>
      <div className="details-wrapper" data-testid="details-wrapper">
        {selectedArticle && <ArticleDetail article={selectedArticle} />}
      </div>
    </div>
  );
}

export default App;
