export const MOCK_ARTICLE_DETAILS = {
  title: "Test Article",
  abstract: "Test abstract",
  published_date: "2024-03-14",
  section: "Test Section",
  subsection: "Test Subsection",
  byline: "Test Author",
  media: [
    {
      type: "image",
      subtype: "",
      caption: "",
      copyright: "Photo illustration by Bráulio Amado",
      approved_for_syndication: 0,
      "media-metadata": [
        {
          url: "https://static01.nyt.com/images/2024/02/25/multimedia/03mag-talk/03mag-talk-thumbStandard-v4.jpg",
          format: "Standard Thumbnail",
          height: 75,
          width: 75,
        },
        {
          url: "https://static01.nyt.com/images/2024/02/25/multimedia/03mag-talk/03mag-talk-mediumThreeByTwo210-v3.jpg",
          format: "mediumThreeByTwo210",
          height: 140,
          width: 210,
        },
        {
          url: "https://static01.nyt.com/images/2024/02/25/multimedia/03mag-talk/03mag-talk-mediumThreeByTwo440-v3.jpg",
          format: "mediumThreeByTwo440",
          height: 293,
          width: 440,
        },
      ],
    },
  ],
  adx_keywords: "keyword1;keyword2",
  des_facet: ["term1", "term2"],
  org_facet: ["organization1", "organization2"],
  per_facet: ["person1", "person2"],
  geo_facet: ["location1", "location2"],
};

export const MOCK_ARTICLE_LIST = [
  { id: 1, title: "Article 1", byline: "Author 1" },
  { id: 2, title: "Article 2", byline: "Author 2" },
  { id: 3, title: "Article 3", byline: "Author 3" },
];

export const MOCK_ARTICLE_LIST_CARD = {
  id: 1,
  title: "Test Article",
  byline: "Test Author",
  published_date: new Date("2024-03-14"),
};
