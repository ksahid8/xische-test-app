export const getArticles = async (url) => {
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`Error fetching articles: Status ${response.status}`);
  }
  const data = await response.json();
  return data;
};
