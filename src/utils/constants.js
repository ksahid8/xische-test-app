import ArticlePlaceholderImage from "assets/article.webp";
import ArticleDetailPlaceholderImage from "assets/articleDetail.webp";
import moment from "moment";

export const API_KEY = "jpI0GATVzMKDjbrM68Tqz2XHXzIrE5RI";
export const API_BASE_URL = "https://api.nytimes.com/svc/mostpopular/v2/viewed";
export const PERIOD = "7";

export const IMAGE_TYPES = {
  list: "Standard Thumbnail",
  detail: "mediumThreeByTwo440",
};

export const ARTICLE_IMAGE = ArticlePlaceholderImage;
export const ARTICLE_DETAIL_IMAGE = ArticleDetailPlaceholderImage;

export const formatDate = (dateString) => {
  const parsedDate = moment(dateString);
  const formattedDate = parsedDate.format("DD MMM YYYY");
  return formattedDate;
};

export const keywordsFormatter = (adx_keywords)=>{
  return adx_keywords.split(";").join(", ")
}

export const getImageUrl = (media, type) => {
  if (!media || media.length === 0) {
    return null;
  }

  const imageArray = media.find((item) => item?.type === "image")?.[
    "media-metadata"
  ];
  if (!imageArray || imageArray.length === 0) {
    return null;
  }

  const imageData = imageArray.find((item) => item?.format === type);
  if (!imageData) {
    return null;
  }

  return imageData.url;
};
