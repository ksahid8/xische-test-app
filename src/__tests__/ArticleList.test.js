import React from "react";
import { render, screen } from "@testing-library/react";
import ArticleList from "containers/ArticleList";
import { MOCK_ARTICLE_LIST } from "utils/fixtures";

describe("ArticleList Component", () => {
  test("renders correct number of ListCard components", () => {
    render(
      <ArticleList
        articles={MOCK_ARTICLE_LIST}
        onArticleClick={() => {}}
        selectedArticle={null}
      />
    );

    const listCards = screen.getAllByTestId("list-card");
    expect(listCards.length).toBe(MOCK_ARTICLE_LIST.length);
  });

  test("passes correct props to each ListCard component", () => {
    render(
      <ArticleList
        articles={MOCK_ARTICLE_LIST}
        onArticleClick={() => {}}
        selectedArticle={null}
      />
    );

    MOCK_ARTICLE_LIST.forEach((article, index) => {
      const listCard = screen.getAllByTestId("list-card")[index];
      expect(listCard).toHaveTextContent(article.title);
      expect(listCard).toHaveTextContent(article.byline);
    });
  });

  test("renders an empty list when no articles are provided", () => {
    render(
      <ArticleList
        articles={[]}
        onArticleClick={() => {}}
        selectedArticle={null}
      />
    );

    const list = screen.getByTestId("article-list");
    expect(list.children.length).toBe(0);
  });
});
