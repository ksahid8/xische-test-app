import { render, screen, waitFor } from "@testing-library/react";
import App from "App";
import React from "react";


describe("App Component", () => {
  beforeEach(() => {
    render(<App />);
  });

  test("renders main wrapper", () => {
    const mainWrapperElement = screen.getByTestId("main-wrapper");
    expect(mainWrapperElement).toBeInTheDocument();
  });

  test("renders list wrapper", () => {
    const listWrapperElement = screen.getByTestId("list-wrapper");
    expect(listWrapperElement).toBeInTheDocument();
  });

  test("renders details wrapper", () => {
    const detailsWrapperElement = screen.getByTestId("details-wrapper");
    expect(detailsWrapperElement).toBeInTheDocument();
  });

  test("renders article list loader", () => {
    const articleListElement = screen.getByTestId("article-list-loader");
    expect(articleListElement).toBeInTheDocument();
  });

  test("renders article list loader initially", () => {
    const articleListLoaderElement = screen.getByTestId("article-list-loader");
    expect(articleListLoaderElement).toBeInTheDocument();
  });

  test("displays article list after loading", async () => {
    await waitFor(() => {
      const articleListElement = screen.queryByTestId("article-list-loader");
      expect(articleListElement).toBeInTheDocument();
    });
  });
});
