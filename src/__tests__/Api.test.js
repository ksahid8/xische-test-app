import { getArticles } from "utils/api";
import { API_BASE_URL } from "utils/constants";

describe("getArticles function", () => {
  global.fetch = jest.fn();

  beforeEach(() => {
    fetch.mockClear();
  });

  const url = API_BASE_URL;

  test("fetches articles successfully", async () => {
    const mockData = { articles: ["article1", "article2"] };
    const mockResponse = {
      ok: true,
      json: jest.fn().mockResolvedValueOnce(mockData),
    };

    fetch.mockResolvedValueOnce(mockResponse);

    const data = await getArticles(url);

    expect(fetch).toHaveBeenCalledWith(url);
    expect(data).toEqual(mockData);
  });

  test("throws error when response is not ok", async () => {
    const mockResponse = {
      ok: false,
      status: 404,
    };

    fetch.mockResolvedValueOnce(mockResponse);

    await expect(getArticles(url)).rejects.toThrow(
      `Error fetching articles: Status ${mockResponse.status}`
    );
  });

  test("throws error when fetch fails", async () => {
    const mockError = new Error("Fetch error");
    fetch.mockRejectedValueOnce(mockError);

    await expect(getArticles(url)).rejects.toThrow(mockError);
  });
});
