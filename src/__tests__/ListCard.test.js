import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import ListCard from "components/ListCard";
import { MOCK_ARTICLE_LIST_CARD } from "utils/fixtures";
import { formatDate } from "utils/constants";

describe("ListCard Component", () => {
  const mockSelectedArticle = {
    id: 1,
  };

  const mockOnArticleClick = jest.fn();

  const { title, byline, published_date, id } = MOCK_ARTICLE_LIST_CARD;

  test("renders correct article details", () => {
    render(
      <ListCard
        article={MOCK_ARTICLE_LIST_CARD}
        onArticleClick={mockOnArticleClick}
        selectedArticle={null}
      />
    );

    expect(screen.getByText(title)).toBeInTheDocument();
    expect(screen.getByText(byline)).toBeInTheDocument();
    expect(screen.getByText(formatDate(published_date))).toBeInTheDocument();
  });

  test("applies 'active' class if article is selected", () => {
    render(
      <ListCard
        article={MOCK_ARTICLE_LIST_CARD}
        onArticleClick={mockOnArticleClick}
        selectedArticle={mockSelectedArticle}
      />
    );

    const listCard = screen.getByTestId("list-card");
    expect(listCard).toHaveClass("active");
  });

  test("does not apply 'active' class if article is not selected", () => {
    render(
      <ListCard
        article={MOCK_ARTICLE_LIST_CARD}
        onArticleClick={mockOnArticleClick}
        selectedArticle={null}
      />
    );

    const listCard = screen.getByTestId("list-card");
    expect(listCard).not.toHaveClass("active");
  });

  test("calls onArticleClick function with correct article ID when clicked", () => {
    render(
      <ListCard
        article={MOCK_ARTICLE_LIST_CARD}
        onArticleClick={mockOnArticleClick}
        selectedArticle={null}
      />
    );

    const listCard = screen.getByTestId("list-card");
    fireEvent.click(listCard);

    expect(mockOnArticleClick).toHaveBeenCalledWith(id);
  });
});
