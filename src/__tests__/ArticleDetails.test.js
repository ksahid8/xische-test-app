import React from "react";
import { render, screen } from "@testing-library/react";
import ArticleDetails from "containers/ArticleDetails";
import { MOCK_ARTICLE_DETAILS } from "utils/fixtures";
import {
  ARTICLE_DETAIL_IMAGE,
  IMAGE_TYPES,
  formatDate,
  getImageUrl,
  keywordsFormatter,
} from "utils/constants";

describe("ArticleDetail Component", () => {
  const {
    title,
    abstract,
    published_date,
    section,
    subsection,
    byline,
    adx_keywords,
    media,
    des_facet,
    org_facet,
    per_facet,
    geo_facet
  } = MOCK_ARTICLE_DETAILS;

  const image = getImageUrl(media, IMAGE_TYPES.detail) ?? ARTICLE_DETAIL_IMAGE;

  test("renders article details", () => {
    render(<ArticleDetails article={MOCK_ARTICLE_DETAILS} />);
    expect(screen.getByText(title)).toBeInTheDocument();
    expect(screen.getByText(abstract)).toBeInTheDocument();
    expect(
      screen.getByText(`Published on ${formatDate(published_date)}`)
    ).toBeInTheDocument();
    expect(screen.getByText(section)).toBeInTheDocument();
    expect(screen.getByText(`(${subsection})`)).toBeInTheDocument();
    expect(screen.getByText(byline)).toBeInTheDocument();
    expect(
      screen.getByText(keywordsFormatter(adx_keywords))
    ).toBeInTheDocument();
  });

  test("renders article detail image ", () => {
    render(<ArticleDetails article={MOCK_ARTICLE_DETAILS} />);
    expect(screen.getByAltText(title)).toHaveAttribute(
      "src",
      image
    );
  });

  test("renders default image if media is empty", () => {
    render(<ArticleDetails article={{ ...MOCK_ARTICLE_DETAILS, media: [] }} />);
    expect(screen.getByAltText("Test Article")).toHaveAttribute(
      "src",
      ARTICLE_DETAIL_IMAGE
    );
  });

  test("renders descriptive terms when data exists", () => {
    render(<ArticleDetails article={MOCK_ARTICLE_DETAILS} />);
    des_facet.forEach((term) => {
      expect(screen.getByText(term)).toBeInTheDocument();
    });
  });

  test("does not render descriptive terms when data does not exist", () => {
    render(<ArticleDetails article={{ ...MOCK_ARTICLE_DETAILS, des_facet: [] }} />);
    expect(screen.queryByText("Descriptive Terms:")).not.toBeInTheDocument();
  });

  test("renders organizations when data exists", () => {
    render(<ArticleDetails article={MOCK_ARTICLE_DETAILS} />);
    org_facet.forEach((term) => {
      expect(screen.getByText(term)).toBeInTheDocument();
    });
  });

  test("does not render organizations when data does not exist", () => {
    render(<ArticleDetails article={{ ...MOCK_ARTICLE_DETAILS, org_facet: [] }} />);
    expect(screen.queryByText("Organizations:")).not.toBeInTheDocument();
  });

  test("renders persons when data exists", () => {
    render(<ArticleDetails article={MOCK_ARTICLE_DETAILS} />);
    per_facet.forEach((term) => {
      expect(screen.getByText(term)).toBeInTheDocument();
    });
  });

  test("does not render persons when data does not exist", () => {
    render(<ArticleDetails article={{ ...MOCK_ARTICLE_DETAILS, per_facet: [] }} />);
    expect(screen.queryByText("Persons:")).not.toBeInTheDocument();
  });

  test("renders locations when data exists", () => {
    render(<ArticleDetails article={MOCK_ARTICLE_DETAILS} />);
    geo_facet.forEach((term) => {
      expect(screen.getByText(term)).toBeInTheDocument();
    });
  });

  test("does not render locations when data does not exist", () => {
    render(<ArticleDetails article={{ ...MOCK_ARTICLE_DETAILS, geo_facet: [] }} />);
    expect(screen.queryByText("Locations:")).not.toBeInTheDocument();
  });

});
