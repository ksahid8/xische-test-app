import React from "react";
import {
  ARTICLE_IMAGE,
  IMAGE_TYPES,
  formatDate,
  getImageUrl,
} from "utils/constants";

const ListCard = ({ article, onArticleClick, selectedArticle }) => {
  const { id, media, title, byline, published_date } = article;
  const image = getImageUrl(media, IMAGE_TYPES.list) ?? ARTICLE_IMAGE;

  return (
    <li
      key={id}
      onClick={() => onArticleClick(id)}
      className={`list-card ${selectedArticle?.id === id && "active"}`}
      data-testid="list-card"
    >
      <div className="image-container">
        <img src={image} alt={selectedArticle?.title} />
      </div>
      <div className="info-container">
        <h4>{title}</h4>
        <div className="info-footer">
          <p className="by-line">{byline}</p>
          <p className="published-on">{formatDate(published_date)}</p>
        </div>
      </div>
    </li>
  );
};

export default ListCard;
