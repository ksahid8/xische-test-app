import React from "react";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

const Loader = () => {
  return (
    <div data-testid="article-list-loader">
      <Skeleton
        count={10}
        height={75}
        width={600}
        className="list-skeleton"
        baseColor="#202020"
        highlightColor="#444"
      />
    </div>
  );
};

export default Loader;
