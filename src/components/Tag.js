import React from "react";

const Tag = ({ item }) => {
  return <span className="tag">{item}</span>;
};

export default Tag;
