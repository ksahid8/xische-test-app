# My React App

Welcome to my React app!

## Getting Started

To get started, follow these steps:

1. Clone this repository to your local machine.
2. Navigate to the project directory in your terminal.
3. Run `npm install` to install project dependencies.

## Available Scripts

In the project directory, you can run the following commands:

### `npm start`

Runs the app in development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

### `npm run test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run coverage`

Runs the test runner with coverage analysis.
This command will display code coverage information after running the tests.

### `npm run cypress:open`

Opens Cypress for running UI tests interactively.
Make sure the development server is running (`npm start`) before opening Cypress.

### `npm run lint`

Runs ESLint to analyze and enforce code quality rules.