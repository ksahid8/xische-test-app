import React from "react";
import ListCard from "../../src/components/ListCard";
import { MOCK_ARTICLE_DETAILS, MOCK_ARTICLE_LIST_CARD } from "utils/fixtures";

describe("<ListCard />", () => {
  beforeEach(() => {
    cy.readFile("./src/App.css").then((cssContent) => {
      cy.document().then((doc) => {
        const style = doc.createElement("style");
        style.innerHTML = cssContent;
        doc.head.appendChild(style);
      });
    });
  });

  it("renders", () => {
    cy.mount(
      <ListCard
        article={MOCK_ARTICLE_LIST_CARD}
        onArticleClick={() => {}}
        selectedArticle={null}
      />
    );
  });

  it("renders with different article data", () => {
    const differentArticleData = MOCK_ARTICLE_DETAILS;

    cy.mount(
      <ListCard
        article={differentArticleData}
        onArticleClick={() => {}}
        selectedArticle={null}
      />
    );
  });

  it("triggers onArticleClick callback on click", () => {
    const onArticleClick = cy.stub().as("articleClick");

    cy.mount(
      <ListCard
        article={MOCK_ARTICLE_LIST_CARD}
        onArticleClick={onArticleClick}
        selectedArticle={null}
      />
    )
      .get(".list-card")
      .click()
      .then(() => {
        expect(onArticleClick).to.be.calledOnce; // eslint-disable-line
        expect(onArticleClick).to.be.calledWith(MOCK_ARTICLE_LIST_CARD.id);
      });
  });

  it('applies "active" class when selectedArticle matches article', () => {
    const selectedArticle = MOCK_ARTICLE_LIST_CARD;

    cy.mount(
      <ListCard
        article={MOCK_ARTICLE_LIST_CARD}
        onArticleClick={() => {}}
        selectedArticle={selectedArticle}
      />
    )
      .get(".list-card")
      .should("have.class", "active");
  });

  it('does not apply "active" class when selectedArticle does not match article', () => {
    const selectedArticle = {
      ...MOCK_ARTICLE_LIST_CARD,
      id: 2,
    };

    cy.mount(
      <ListCard
        article={MOCK_ARTICLE_LIST_CARD}
        onArticleClick={() => {}}
        selectedArticle={selectedArticle}
      />
    )
      .get(".list-card")
      .should("not.have.class", "active");
  });
});
