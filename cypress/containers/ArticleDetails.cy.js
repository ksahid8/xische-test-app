import React from "react";
import ArticleDetails from "../../src/containers/ArticleDetails";
import { MOCK_ARTICLE_DETAILS } from "utils/fixtures";
import {
  ARTICLE_DETAIL_IMAGE,
  IMAGE_TYPES,
  formatDate,
  getImageUrl,
} from "utils/constants";

describe("ArticleDetail Component", () => {
  const {
    title,
    abstract,
    published_date,
    section,
    subsection,
    byline,
    media,
    des_facet,
    org_facet,
    per_facet,
    geo_facet,
  } = MOCK_ARTICLE_DETAILS;

  const image = getImageUrl(media, IMAGE_TYPES.detail) ?? ARTICLE_DETAIL_IMAGE;

  beforeEach(() => {
    cy.readFile("./src/App.css").then((cssContent) => {
      cy.document().then((doc) => {
        const style = doc.createElement("style");
        style.innerHTML = cssContent;
        doc.head.appendChild(style);
      });
    });

    cy.mount(<ArticleDetails article={MOCK_ARTICLE_DETAILS} />);
  });

  it("renders article details correctly", () => {
    cy.get('[data-testid="article-details"]').should("exist");

    cy.get(".detail-title").should("contain.text", title);

    cy.get(".detail-image").should("have.attr", "src").and("include", image);
    cy.get(".detail-image").should("have.attr", "alt").and("include", title);

    cy.get(".section-text").should("contain.text", section);
    cy.get(".sub-section-text").should("contain.text", `(${subsection})`);

    cy.get(".detail-info-published").should(
      "contain.text",
      `Published on ${formatDate(published_date)}`
    );
    cy.get(".detail-info-by-line").should("contain.text", byline);

    cy.get(".detail-text").should("contain.text", abstract);

    cy.get(".detail-section")
      .contains("Descriptive Terms:")
      .parent()
      .within(() => {
        cy.get(".detail-tags")
          .children(".tag")
          .should("have.length", des_facet.length);
      });

    cy.get(".detail-section")
      .contains("Organizations:")
      .parent()
      .within(() => {
        cy.get(".detail-tags")
          .children(".tag")
          .should("have.length", org_facet.length);
      });

    cy.get(".detail-section")
      .contains("Persons:")
      .parent()
      .within(() => {
        cy.get(".detail-tags")
          .children(".tag")
          .should("have.length", per_facet.length);
      });

    cy.get(".detail-section")
      .contains("Locations:")
      .parent()
      .within(() => {
        cy.get(".detail-tags")
          .children(".tag")
          .should("have.length", geo_facet.length);
      });
  });
});
